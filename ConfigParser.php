<?php

include("ConfigElement.php");
include("TodoParser.php");

class ConfigParser
{
	private $elements;

	function __construct()
	{
		$this->elements = array();
	}

	function parse($path)
	{
		$fh = fopen($path, "r");
		while (($line= fgets($fh)) !== FALSE)
		{
			$data = explode(',', $line);
			if (count($data) != 2)
				continue;

			$this->elements[] = new ConfigElement($data[0], $data[1]);
		}
		fclose($fh);
	}

	function writeConfig()
	{
		foreach ($this->elements as $element) {
			$element->write();
		}
	}

	function getConfigCount()
	{
		return count($this->elements);
	}

	function readFiles()
	{
        $files = array();
		foreach ($this->elements as $element)
		{
			switch($element->getKey()) {
				case 'todo':
					$parser = new TodoParser($element->getValue());
                    $files[] = $parser->parseFile();
					break;
			}
		}
		return $files;
	}

};
?>
