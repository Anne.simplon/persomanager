<?php


class TodoParser
{
    private $fileName;

    function __construct($fileName)
    {
        $this->elements = array();
        $this->fileName = $fileName;
    }

    public function parseFile()
    {
        $txt_file = file_get_contents($this->fileName);
        if(!$txt_file) {
            return null;
        }
        $rows        = explode("\n", $txt_file);
        $file = new CsvFile($rows[0]);
        array_shift($rows);

        foreach($rows as $row => $data) {
            $file->addRow($data);
        }

        return $file;
    }
}
?>